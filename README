NASPRO Bridge it
================

  http://naspro.sourceforge.net/libraries.html#naspro-bridge-it

  Version: 0.5.1, API: 3.0.0

About
-----

  NASPRO Bridge it is a little helper library to develop insert-your-API-here to
  LV2 bridges.

  As of now, it basically offers a few utility functions and Turtle/RDF
  serialization for LV2 dynamic manifest generation, supporting the following
  LV2 extensions:

    * LV2 (http://lv2plug.in/ns/lv2core/) >= 4.0;
    * LV2 Dynamic Manifest (http://lv2plug.in/ns/ext/dynmanifest/) >= 1.0;
    * LV2 Port Properties (http://lv2plug.in/ns/ext/port-props/) >= 1.0 (only
      for the logarithmic port property);
    * LV2 Atom (http://lv2plug.in/ns/ext/atom/) >= 1.0;
    * LV2 MIDI (http://lv2plug.in/ns/ext/midi/) >= 1.0;
    * LV2 URID (http://lv2plug.in/ns/ext/urid/) >= 1.2;
    * LV2 Presets (http://lv2plug.in/ns/ext/presets/) >= 2.6.

  The code is released under the LGPL 2.1
  (http://www.gnu.org/licenses/lgpl-2.1.html).

Supported platforms/compilers
-----------------------------

  It is written in C99 and there is no platform- or compiler-specific code,
  hence wherever NASPRO core
  (http://naspro.sourceforge.net/libraries.html#naspro-core) runs, this library
  is able to run.

Runtime dependencies
--------------------

  * NASPRO core >= 0.4.0.

Build-time dependencies
-----------------------

  * LV2 (http://lv2plug.in/) >= 1.0.0;
  * pkg-config (http://pkg-config.freedesktop.org/);
  * An environment capable of running Autotools-based build systems;
  * (optional) GNU Autoconf (http://www.gnu.org/software/autoconf/) >= 2.69, GNU
    Automake (http://www.gnu.org/software/automake/) and GNU Libtool
    (http://www.gnu.org/software/libtool/) to regenerate the build system;
  * (optional) Natural Docs (http://www.naturaldocs.org/) >= 1.5 to regenerate
    the build system and/or the documentation.

Usage
-----

  Just include <NASPRO/brit/lib.h> in your source files and use the pkg-config
  tool to retrieve compiler and linker flags (package name: nabrit-3).

Installation
------------

  As usual:

  $ ./configure && make && make install

  Extra ./configure options
  -------------------------

    --with-pkgconfigdir=DIR where to put pkg-config's .pc files
                            [LIBDIR/pkgconfig]
