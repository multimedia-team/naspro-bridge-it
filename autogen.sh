#!/bin/sh

mkdir -p doc/html
aclocal
autoheader
autoconf
libtoolize --copy
automake --add-missing --copy
