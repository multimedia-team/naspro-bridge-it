/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#include "internal.h"

_NABRIT_DEF nabrit_port
nabrit_port_new(nabrit_plugin plugin, const char *symbol, nabrit_port_type type,
		nabrit_port_direction direction)
{
	struct _nabrit_port port;
	nacore_list_elem elem;

	port.symbol		= symbol;
	port.type		= type;
	port.direction		= direction;
	port.name		= NULL;
	port.reports_latency	= 0;
	port.toggled		= 0;
	port.sample_rate	= 0;
	port.integer		= 0;
	port.logarithmic	= 0;
	port.min		= NAN;
	port.max		= NAN;
	port.deflt		= NAN;

	elem = nacore_list_append(plugin->ports, NULL, &port);
	if (elem == NULL)
		return NULL;

	return (nabrit_port)nacore_list_elem_get_value(plugin->ports, elem);
}

_NABRIT_DEF const char *
nabrit_port_get_symbol(nabrit_port port)
{
	return port->symbol;
}

_NABRIT_DEF nabrit_port_type
nabrit_port_get_type(nabrit_port port)
{
	return port->type;
}

_NABRIT_DEF void
nabrit_port_set_name(nabrit_port port, const char *name)
{
	port->name = name;
}

_NABRIT_DEF void
nabrit_port_set_reports_latency(nabrit_port port, char reports_latency)
{
	port->reports_latency = reports_latency;
}

_NABRIT_DEF void
nabrit_port_set_toggled(nabrit_port port, char toggled)
{
	port->toggled = toggled;
}

_NABRIT_DEF void
nabrit_port_set_sample_rate(nabrit_port port, char sample_rate)
{
	port->sample_rate = sample_rate;
}

_NABRIT_DEF void
nabrit_port_set_integer(nabrit_port port, char integer)
{
	port->integer = integer;
}

_NABRIT_DEF void
nabrit_port_set_logarithmic(nabrit_port port, char logarithmic)
{
	port->logarithmic = logarithmic;
}

_NABRIT_DEF void
nabrit_port_set_min(nabrit_port port, float min)
{
	port->min = min;
}

_NABRIT_DEF void
nabrit_port_set_max(nabrit_port port, float max)
{
	port->max = max;
}

_NABRIT_DEF void
nabrit_port_set_deflt(nabrit_port port, float deflt)
{
	port->deflt = deflt;
}

NACORE_PRIVATE size_t
_nabrit_port_get_size(const void *value, void *opaque)
{
	return sizeof(struct _nabrit_port);
}
