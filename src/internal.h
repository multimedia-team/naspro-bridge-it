/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#ifndef _NABRIT_INTERNAL_H
#define _NABRIT_INTERNAL_H

#include <stdlib.h>
#include <string.h>

#include <math.h>

#include <NASPRO/brit/lib.h>

#include "bridge.h"
#include "pluglib.h"
#include "plugin.h"
#include "port.h"
#include "preset.h"

#endif
