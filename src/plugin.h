/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#ifndef _NABRIT_PLUGIN_H
#define _NABRIT_PLUGIN_H

struct _nabrit_plugin
  {
	LV2_Descriptor	 descriptor;
	void		*opaque;
	const char	*name;
	const char	*creator;
	const char	*rights;
	char		 is_live;
	char		 in_place_broken;
	char		 hard_rt_capable;
	nacore_list	 ports;
	nacore_list	 presets;
  };

NACORE_PRIVATE size_t
_nabrit_plugin_get_size(const void *value, void *opaque);

NACORE_PRIVATE int
_nabrit_plugin_cmp(const void *v1, const void *v2, void *opaque);

#endif
