/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#include "internal.h"

_NABRIT_DEF char
nabrit_util_filter_by_suffix(const void *value, void *opaque)
{
	nacore_fs_dir_entry entry;
	const char *suffix;
	const char *s;
	size_t len_s, len_sfx;

	entry = (nacore_fs_dir_entry)value;
	suffix = (const char *)opaque;

	s = nacore_fs_dir_entry_get_name(entry);

	len_s = strlen(s);
	len_sfx = strlen(suffix);

	if (len_s < len_sfx)
		return 0;

	return (strcmp(s + len_s - len_sfx, suffix) == 0);
}

_NABRIT_DEF int
nabrit_util_load_all_in_env_path(nabrit_bridge bridge, const char *name,
				 nacore_filter_cb dir_entry_filter_cb,
				 void *dir_entry_filter_opaque,
				 nacore_op_cb load_cb, void *load_opaque)
{
	const char *path;
	nacore_list prefixes;
	nacore_list_elem elem;
	const char *p;
	int errsv;

	path = nacore_env_get(name);
	if (path == NULL)
	  {
		errsv = errno;
		goto path_err;
	  }

	prefixes = nacore_env_path_prefixes_split(path);
	if (prefixes == NULL)
	  {
		errsv = ENOMEM;
		goto prefixes_err;
	  }

	nacore_env_free(path);

	for (elem = nacore_list_get_head(prefixes); elem != NULL;
	     elem = nacore_list_elem_get_next(prefixes, elem))
	  {
		p = nacore_list_elem_get_value(prefixes, elem);

		nabrit_util_load_all_in_dir(bridge, p, dir_entry_filter_cb,
					    dir_entry_filter_opaque, load_cb,
					    load_opaque);
	  }

	nacore_list_free(prefixes, NULL, NULL);

	return 0;

prefixes_err:
	nacore_env_free(path);
path_err:
	return errsv;
}

_NABRIT_DEF int
nabrit_util_load_all_in_dir(nabrit_bridge bridge, const char *dirname,
			    nacore_filter_cb dir_entry_filter_cb,
			    void *dir_entry_filter_opaque, nacore_op_cb load_cb,
			    void *load_opaque)
{
	nacore_fs_dir dir;
	nacore_fs_dir_entry entry;
	const char *name;
	char *filename;

	dir = nacore_fs_dir_open(dirname);
	if (dir == NULL)
		return errno;

	for (entry = nacore_fs_dir_get_next_entry(dir); entry != NULL;
	     nacore_fs_dir_entry_free(entry),
	     entry = nacore_fs_dir_get_next_entry(dir))
	  {
		name = nacore_fs_dir_entry_get_name(entry);

		if (dir_entry_filter_cb != NULL)
			if (dir_entry_filter_cb(entry, dir_entry_filter_opaque)
			    == 0)
				continue;

		nacore_asprintf_nl(&filename, "%s%s%s", dirname,
				   nacore_fs_dir_sep, name);
		if (filename == NULL)
			continue;

		load_cb(filename, load_opaque);
	  }

	nacore_fs_dir_close(dir);

	return 0;
}
