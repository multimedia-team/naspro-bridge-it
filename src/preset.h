/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#ifndef _NABRIT_PRESET_H
#define _NABRIT_PRESET_H

struct _nabrit_preset
  {
	const char	*name;
	nacore_list	 values;
  };

typedef struct
  {
	nabrit_port	port;
	float		value;
  } preset_value_t;

NACORE_PRIVATE size_t
_nabrit_preset_get_size(const void *value, void *opaque);

#endif
