/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#include "internal.h"

static void
print_uriref(FILE *fp, const char *URI)
{
	uint32_t cp;
	size_t len;

	while (*URI != '\0')
	  {
		len = nacore_char_utf8_decode(URI, &cp);

		if (cp < 0x20)
		  {
			fprintf(fp, "\\u00%X%X", (cp & 0xf0) >> 4, cp & 0xf);
			URI++;
			continue;
		  }

		if (cp == '>')
		  {
			fputc('\\', fp);
			fputc('>', fp);
			URI++;
			continue;
		  }

		for (; len > 0; URI++, len--)
			fputc(*URI, fp);

		if (cp == '\\')
			fputc('\\', fp);
	  }
}

static void
print_string(FILE *fp, const char *string)
{
	uint32_t cp;
	size_t len;

	while (*string != '\0')
	  {
		len = nacore_char_utf8_decode(string, &cp);

		if (cp < 0x20)
		  {
			fprintf(fp, "\\u00%X%X", (cp & 0xf0) >> 4, cp & 0xf);
			string++;
			continue;
		  }

		if (cp == '"')
		  {
			fputc('\\', fp);
			fputc('"', fp);
			string++;
			continue;
		  }

		for (; len > 0; string++, len--)
			fputc(*string, fp);

		if (cp == '\\')
			fputc('\\', fp);
	  }
}

_NABRIT_DEF void
nabrit_manifest_print_subjects(nabrit_bridge bridge, FILE *fp)
{
	nacore_avl_tree_elem elem;
	nacore_list_elem pelem;
	nabrit_plugin plugin;
	size_t i;

	fprintf(fp, "@prefix lv2:  <http://lv2plug.in/ns/lv2core#> .\n");
	fprintf(fp, "@prefix pset: <http://lv2plug.in/ns/ext/presets#> .\n");

	for (elem = nacore_avl_tree_get_first(bridge->plugins); elem != NULL;
	     elem = nacore_avl_tree_elem_get_next(bridge->plugins, elem))
	  {
		plugin = (nabrit_plugin)nacore_avl_tree_elem_get_value(
							bridge->plugins, elem);
		fprintf(fp, "<");
		print_uriref(fp, plugin->descriptor.URI);
		fprintf(fp, "> a lv2:Plugin .\n");

		for (i = 0, pelem = nacore_list_get_head(plugin->presets);
		     pelem != NULL;
		     i++, pelem = nacore_list_elem_get_next(plugin->presets,
							    pelem))
		  {
			fprintf(fp, "<");
			print_uriref(fp, plugin->descriptor.URI);
			fprintf(fp, "/presets/preset%"
				    NACORE_LIBC_SIZE_FORMAT_LM "u> a "
				    "pset:Preset ;\n",
				    (NACORE_LIBC_SIZE_FORMAT_TYPE)i);

			fprintf(fp, "\tlv2:appliesTo <");
			print_uriref(fp, plugin->descriptor.URI);
			fprintf(fp, "> .\n");
		  }
	  }
}

_NABRIT_DEF int
nabrit_manifest_print_data(nabrit_bridge bridge, FILE *fp, const char *URI)
{
	nacore_list_elem lelem, lelem2;
	nacore_avl_tree_elem telem;
	struct _nabrit_plugin p;
	nabrit_plugin plugin;
	nabrit_port port;
	nabrit_preset preset;
	preset_value_t *pset_value;
	size_t i, j;
	nacore_locale c_loc, cur_loc;

	p.descriptor.URI = URI;

	telem = nacore_avl_tree_find(bridge->plugins, NULL, NULL, NULL, &p);
	if (telem == NULL)
		return ENOENT;

	c_loc = nacore_locale_new(NACORE_LOCALE_NUMERIC_MASK, "C",
				  (nacore_locale)0);
	if (c_loc == (nacore_locale)0)
		return ENOMEM;

	cur_loc = nacore_locale_use(c_loc);

	plugin = (nabrit_plugin)nacore_avl_tree_elem_get_value(bridge->plugins,
							       telem);

	fprintf(fp, "@prefix lv2:    <http://lv2plug.in/ns/lv2core#> .\n");
	fprintf(fp, "@prefix rdfs:   <http://www.w3.org/2000/01/rdf-schema#> "
		".\n");
	fprintf(fp, "@prefix doap:   <http://usefulinc.com/ns/doap#> .\n");
	fprintf(fp, "@prefix dc:     <http://purl.org/dc/elements/1.1/> .\n");
	fprintf(fp, "@prefix pprops: <http://lv2plug.in/ns/ext/port-props#> "
		".\n");
	fprintf(fp, "@prefix atom:   <http://lv2plug.in/ns/ext/atom#> .\n");
	fprintf(fp, "@prefix midi:   <http://lv2plug.in/ns/ext/midi#> .\n");
	fprintf(fp, "@prefix urid:   <http://lv2plug.in/ns/ext/urid#> .\n");
	fprintf(fp, "@prefix pset:   <http://lv2plug.in/ns/ext/presets#> .\n");

	fprintf(fp, "<");
	print_uriref(fp, plugin->descriptor.URI);
	fprintf(fp, "> a lv2:Plugin ;\n");

	fprintf(fp, "\tdoap:name \"");
	if (plugin->name != NULL)
		print_string(fp, plugin->name);
	else
		fprintf(fp, "(no name)");
	fprintf(fp, "\" ;\n");

	fprintf(fp, "\tlv2:minorVersion 0 ;\n");
	fprintf(fp, "\tlv2:microVersion 0 ;\n");

	fprintf(fp, "\tlv2:binary <");
	print_uriref(fp, bridge->binary);
	fprintf(fp, ">");

	if (plugin->creator != NULL)
	  {
		fprintf(fp, " ;\n\tdc:creator \"");
		print_string(fp, plugin->creator);
		fprintf(fp, "\"");
	  }
	if (plugin->rights != NULL)
	  {
		fprintf(fp, " ;\n\tdc:rights \"");
		print_string(fp, plugin->rights);
		fprintf(fp, "\"");
	  }

	if (plugin->is_live != 0)
		fprintf(fp, " ;\n\tlv2:requiredFeature lv2:isLive");
	if (plugin->in_place_broken != 0)
		fprintf(fp, " ;\n\tlv2:requiredFeature lv2:inPlaceBroken");
	if (plugin->hard_rt_capable != 0)
		fprintf(fp, " ;\n\tlv2:optionalFeature lv2:hardRTCapable");

	for (lelem = nacore_list_get_head(plugin->ports); lelem != NULL;
	     lelem = nacore_list_elem_get_next(plugin->ports, lelem))
	  {
		port = nacore_list_elem_get_value(plugin->ports, lelem);

		if (port->type == nabrit_port_type_midi)
		  {
			fprintf(fp, " ;\n\tlv2:requiredFeature urid:map");
			break;
		  }
	  }

	for (lelem = nacore_list_get_head(plugin->ports); lelem != NULL;
	     lelem = nacore_list_elem_get_next(plugin->ports, lelem))
	  {
		port = nacore_list_elem_get_value(plugin->ports, lelem);

		fprintf(fp, " ;\n\tlv2:port <");
		print_uriref(fp, plugin->descriptor.URI);
		fprintf(fp, "/ports/");
		print_uriref(fp, port->symbol);
		fprintf(fp, ">");
	  }

	fprintf(fp, "\n.\n");

	for (i = 0, lelem = nacore_list_get_head(plugin->ports); lelem != NULL;
	     i++, lelem = nacore_list_elem_get_next(plugin->ports, lelem))
	  {
		port = nacore_list_elem_get_value(plugin->ports, lelem);

		fprintf(fp, "<");
		print_uriref(fp, plugin->descriptor.URI);
		fprintf(fp, "/ports/");
		print_uriref(fp, port->symbol);
		fprintf(fp, "> a lv2:Port ;\n");

		if (port->type == nabrit_port_type_audio)
			fprintf(fp, "\ta lv2:AudioPort ;\n");
		else if (port->type == nabrit_port_type_control)
			fprintf(fp, "\ta lv2:ControlPort ;\n");
		else
		  {
			fprintf(fp, "\ta atom:AtomPort ;\n");
			fprintf(fp, "\tatom:bufferType atom:Sequence ;\n");
			fprintf(fp, "\tatom:supports midi:MidiEvent ;\n");
		  }

		if (port->direction == nabrit_port_direction_in)
			fprintf(fp, "\ta lv2:InputPort ;\n");
		else
			fprintf(fp, "\ta lv2:OutputPort ;\n");

		fprintf(fp, "\tlv2:index %" NACORE_LIBC_SIZE_FORMAT_LM "u ;\n",
			(NACORE_LIBC_SIZE_FORMAT_TYPE)i);

		fprintf(fp, "\tlv2:symbol \"");
		print_string(fp, port->symbol);
		fprintf(fp, "\"");

		if (port->name != NULL)
		  {
			fprintf(fp, " ;\n\tlv2:name \"");
			print_string(fp, port->name);
			fprintf(fp, "\"");
		  }
		else
			fprintf(fp, " ;\n\tlv2:name \"(no name)\"");

		if (port->reports_latency)
			fprintf(fp, " ;\n\tlv2:portProperty "
				"lv2:reportsLatency");

		if (port->toggled)
			fprintf(fp, " ;\n\tlv2:portProperty lv2:toggled");

		if (port->sample_rate)
			fprintf(fp, " ;\n\tlv2:portProperty lv2:sampleRate");

		if (port->integer)
			fprintf(fp, " ;\n\tlv2:portProperty lv2:integer");

		if (port->logarithmic)
			fprintf(fp, " ;\n\tlv2:portProperty "
				"pprops:logarithmic");

		if (!isnan(port->min))
			fprintf(fp, " ;\n\tlv2:minimum %g", port->min);

		if (!isnan(port->max))
			fprintf(fp, " ;\n\tlv2:maximum %g", port->max);

		if (!isnan(port->deflt))
			fprintf(fp, " ;\n\tlv2:default %g", port->deflt);

		fprintf(fp, "\n.\n");
	  }

	for (i = 0, lelem = nacore_list_get_head(plugin->presets);
	     lelem != NULL;
	     i++, lelem = nacore_list_elem_get_next(plugin->presets, lelem))
	  {
		preset = nacore_list_elem_get_value(plugin->presets, lelem);

		fprintf(fp, "\n<");
		print_uriref(fp, plugin->descriptor.URI);
		fprintf(fp, "/presets/preset%" NACORE_LIBC_SIZE_FORMAT_LM "u> "
			"a pset:Preset ;\n", (NACORE_LIBC_SIZE_FORMAT_TYPE)i);

		fprintf(fp, "\trdfs:label \"");
		print_string(fp, preset->name);
		fprintf(fp, "\" ;\n");

		fprintf(fp, "\tlv2:appliesTo <");
		print_uriref(fp, plugin->descriptor.URI);
		fprintf(fp, "> ;\n");

		fprintf(fp, "\tpset:appliesTo <");
		print_uriref(fp, plugin->descriptor.URI);
		fprintf(fp, ">");

		for (j = 0, lelem2 = nacore_list_get_head(preset->values);
		     lelem2 != NULL;
		     j++, lelem2 = nacore_list_elem_get_next(preset->values,
		     					     lelem2))
		  {
			pset_value = nacore_list_elem_get_value(preset->values,
								lelem2);

			fprintf(fp, " ;\n");

			fprintf(fp, "\tlv2:port [\n");
			fprintf(fp, "\t\tlv2:symbol \"");
			print_string(fp, pset_value->port->symbol);
			fprintf(fp, "\" ;\n");
			fprintf(fp, "\t\tpset:value %g\n", pset_value->value);
			fprintf(fp, "\t]");
		  }

		fprintf(fp, "\n.\n");
	  }

	nacore_locale_use(cur_loc);
	nacore_locale_free(c_loc);

	return 0;
}
