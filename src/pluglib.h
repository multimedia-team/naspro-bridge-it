/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#ifndef _NABRIT_PLUGLIB_H
#define _NABRIT_PLUGLIB_H

struct _nabrit_pluglib
  {
	nacore_list	 plugins;
	const char	*filename;
	void		*opaque;
  };

NACORE_PRIVATE size_t
_nabrit_pluglib_get_size(const void *value, void *opaque);

#endif
