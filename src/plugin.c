/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#include "internal.h"

_NABRIT_DEF nabrit_plugin
nabrit_plugin_new(nabrit_bridge bridge, nabrit_pluglib pluglib,
		  const LV2_Descriptor *descriptor)
{
	struct _nabrit_plugin plugin;
	nabrit_plugin ret;
	nacore_avl_tree_elem telem;
	nacore_list_elem lelem;

	plugin.descriptor	= *descriptor;
	plugin.opaque		= NULL;
	plugin.name		= NULL;
	plugin.creator		= NULL;
	plugin.rights		= NULL;
	plugin.is_live		= 0;
	plugin.in_place_broken	= 0;
	plugin.hard_rt_capable	= 0;

	if (nacore_avl_tree_find(bridge->plugins, NULL, NULL, NULL, &plugin)
	    != NULL)
	  {
		errno = EEXIST;
		return NULL;
	  }

	plugin.ports = nacore_list_new(_nabrit_port_get_size);
	if (plugin.ports == NULL)
		goto ports_err;

	plugin.presets = nacore_list_new(_nabrit_preset_get_size);
	if (plugin.presets == NULL)
		goto presets_err;

	lelem = nacore_list_append(pluglib->plugins, NULL, &plugin);
	if (lelem == NULL)
		goto pluglib_err;

	ret = nacore_list_elem_get_value(pluglib->plugins, lelem);

	telem = nacore_avl_tree_insert(bridge->plugins, NULL, NULL, ret);
	if (telem == NULL)
		goto bridge_err;

	return ret;

bridge_err:
	free(nacore_list_pop(pluglib->plugins, lelem));
pluglib_err:
	nacore_list_free(plugin.presets, NULL, NULL);
presets_err:
	nacore_list_free(plugin.ports, NULL, NULL);
ports_err:
	errno = ENOMEM;
	return NULL;
}

_NABRIT_DEF void
nabrit_plugin_free(nabrit_bridge bridge, nabrit_pluglib pluglib,
		   nabrit_plugin plugin)
{
	nacore_list_elem lelem;
	nacore_avl_tree_elem telem;

	lelem = nacore_list_find_first(pluglib->plugins, _nabrit_plugin_cmp,
				       NULL, NULL, NULL, plugin);
	nacore_list_pop(pluglib->plugins, lelem);

	telem = nacore_avl_tree_find(bridge->plugins, NULL, NULL, NULL, plugin);
	nacore_avl_tree_pop(bridge->plugins, telem);

	free(plugin);
}

_NABRIT_DEF void
nabrit_plugin_free_ports(nabrit_plugin plugin, nacore_op_cb free_cb,
			 void *free_opaque)
{
	nacore_list_free(plugin->ports, free_cb, free_opaque);
}

typedef struct
  {
	nacore_op_cb	 free_cb;
	void		*free_opaque;
  } preset_free_data_t;

static void
preset_free_cb(void *value, void *opaque)
{
	nabrit_preset p;
	preset_free_data_t *d;

	p = (nabrit_preset)value;
	d = (preset_free_data_t *)opaque;

	nacore_list_free(p->values, NULL, NULL);

	if (d->free_cb != NULL)
		d->free_cb(p, d->free_opaque);
}

_NABRIT_DEF void
nabrit_plugin_free_presets(nabrit_plugin plugin, nacore_op_cb free_cb,
			   void *free_opaque)
{
	preset_free_data_t opaque;

	opaque.free_cb		= free_cb;
	opaque.free_opaque	= free_opaque;

	nacore_list_free(plugin->presets, preset_free_cb, &opaque);
}

_NABRIT_DEF nabrit_plugin
nabrit_plugin_from_descriptor(const LV2_Descriptor *descriptor)
{
	return (nabrit_plugin)descriptor;
}

_NABRIT_DEF LV2_Descriptor *
nabrit_plugin_get_descriptor(nabrit_plugin plugin)
{
	return &plugin->descriptor;
}

_NABRIT_DEF void *
nabrit_plugin_get_opaque(nabrit_plugin plugin)
{
	return plugin->opaque;
}

_NABRIT_DEF void
nabrit_plugin_set_opaque(nabrit_plugin plugin, void *opaque)
{
	plugin->opaque = opaque;
}

_NABRIT_DEF void
nabrit_plugin_set_name(nabrit_plugin plugin, const char *name)
{
	plugin->name = name;
}

_NABRIT_DEF void
nabrit_plugin_set_creator(nabrit_plugin plugin, const char *creator)
{
	plugin->creator = creator;
}

_NABRIT_DEF void
nabrit_plugin_set_rights(nabrit_plugin plugin, const char *rights)
{
	plugin->rights = rights;
}

_NABRIT_DEF void
nabrit_plugin_set_is_live(nabrit_plugin plugin, char is_live)
{
	plugin->is_live = is_live;
}

_NABRIT_DEF void
nabrit_plugin_set_in_place_broken(nabrit_plugin plugin, char in_place_broken)
{
	plugin->in_place_broken = in_place_broken;
}

_NABRIT_DEF void
nabrit_plugin_set_hard_rt_capable(nabrit_plugin plugin, char hard_rt_capable)
{
	plugin->hard_rt_capable = hard_rt_capable;
}

NACORE_PRIVATE size_t
_nabrit_plugin_get_size(const void *value, void *opaque)
{
	return sizeof(struct _nabrit_plugin);
}

NACORE_PRIVATE int
_nabrit_plugin_cmp(const void *v1, const void *v2, void *opaque)
{
	nabrit_plugin p1, p2;

	p1 = (nabrit_plugin)v1;
	p2 = (nabrit_plugin)v2;

	return strcmp(p1->descriptor.URI, p2->descriptor.URI);
}
