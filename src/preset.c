/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#include "internal.h"

static size_t
preset_value_get_size(const void *value, void *opaque)
{
	return sizeof(preset_value_t);
}

_NABRIT_DEF nabrit_preset
nabrit_preset_new(nabrit_plugin plugin, const char *name)
{
	struct _nabrit_preset preset;
	nacore_list_elem elem;

	preset.name = name;

	preset.values = nacore_list_new(preset_value_get_size);
	if (preset.values == NULL)
		return NULL;

	elem = nacore_list_append(plugin->presets, NULL, &preset);
	if (elem == NULL)
	  {
		nacore_list_free(preset.values, NULL, NULL);
		return NULL;
	  }

	return (nabrit_preset)nacore_list_elem_get_value(plugin->presets, elem);
}

_NABRIT_DEF int
nabrit_preset_add_value(nabrit_preset preset, nabrit_port port, float value)
{
	preset_value_t pval;

	pval.port  = port;
	pval.value = value;

	if (nacore_list_append(preset->values, NULL, &pval) == NULL)
		return ENOMEM;

	return 0;
}

_NABRIT_DEF const char *
nabrit_preset_get_name(nabrit_preset preset)
{
	return preset->name;
}

NACORE_PRIVATE size_t
_nabrit_preset_get_size(const void *value, void *opaque)
{
	return sizeof(struct _nabrit_preset);
}
