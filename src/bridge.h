/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#ifndef _NABRIT_BRIDGE_H
#define _NABRIT_BRIDGE_H

struct _nabrit_bridge
  {
	nacore_list	 pluglibs;
	nacore_avl_tree	 plugins;
	const char	*binary;
  };

#endif
