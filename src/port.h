/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#ifndef _NABRIT_PORT_H
#define _NABRIT_PORT_H

struct _nabrit_port
  {
	const char		*symbol;
	const char		*name;
	nabrit_port_type	 type;
	nabrit_port_direction	 direction;
	char			 reports_latency;
	char			 toggled;
	char			 sample_rate;
	char			 integer;
	char			 logarithmic;
	float			 min;
	float			 max;
	float			 deflt;
  };

NACORE_PRIVATE size_t
_nabrit_port_get_size(const void *value, void *opaque);

#endif
