/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#include "internal.h"

_NABRIT_DEF nabrit_pluglib
nabrit_pluglib_new(nabrit_bridge bridge, const char *filename)
{
	struct _nabrit_pluglib pluglib;
	nabrit_pluglib ret;
	nacore_list_elem elem;

	pluglib.plugins = nacore_list_new(_nabrit_plugin_get_size);
	if (pluglib.plugins == NULL)
		return NULL;

	pluglib.filename	= filename;
	pluglib.opaque		= NULL;

	elem = nacore_list_append(bridge->pluglibs, NULL, &pluglib);
	if (elem == NULL)
	  {
		nacore_list_free(pluglib.plugins, NULL, NULL);
		return NULL;
	  }

	ret = nacore_list_elem_get_value(bridge->pluglibs, elem);

	return ret;
}

_NABRIT_DEF void
nabrit_pluglib_free_plugins(nabrit_bridge bridge, nabrit_pluglib pluglib,
			    nacore_op_cb free_cb, void *free_opaque)
{
	nacore_list_free(pluglib->plugins, free_cb, free_opaque);
}

_NABRIT_DEF const char *
nabrit_pluglib_get_filename(nabrit_pluglib pluglib)
{
	return pluglib->filename;
}

_NABRIT_DEF void *
nabrit_pluglib_get_opaque(nabrit_pluglib pluglib)
{
	return pluglib->opaque;
}

_NABRIT_DEF void
nabrit_pluglib_set_opaque(nabrit_pluglib pluglib, void *opaque)
{
	pluglib->opaque = opaque;
}

NACORE_PRIVATE size_t
_nabrit_pluglib_get_size(const void *value, void *opaque)
{
	return sizeof(struct _nabrit_pluglib);
}
