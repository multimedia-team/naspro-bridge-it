/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#include "internal.h"

_NABRIT_DEF nabrit_bridge
nabrit_bridge_new(const char *binary)
{
	nabrit_bridge ret;

	ret = malloc(sizeof(struct _nabrit_bridge));
	if (ret == NULL)
		goto alloc_err;

	ret->pluglibs = nacore_list_new(_nabrit_pluglib_get_size);
	if (ret == NULL)
		goto pluglibs_err;

	ret->plugins = nacore_avl_tree_new(_nabrit_plugin_cmp, NULL);
	if (ret->plugins == NULL)
		goto plugins_err;

	ret->binary = binary;

	return ret;

plugins_err:
	nacore_list_free(ret->pluglibs, NULL, NULL);
pluglibs_err:
	free(ret);
alloc_err:
	return NULL;
}

_NABRIT_DEF void
nabrit_bridge_free(nabrit_bridge bridge, nacore_op_cb free_cb,
		   void *free_opaque)
{
	nacore_list_free(bridge->pluglibs, free_cb, free_opaque);
	nacore_avl_tree_free(bridge->plugins, NULL, NULL);

	free(bridge);
}

_NABRIT_DEF const LV2_Descriptor *
nabrit_bridge_get_descriptor(nabrit_bridge bridge, uint32_t index)
{
	nacore_avl_tree_elem elem;

	if (index >= nacore_avl_tree_get_n_elems(bridge->plugins))
		return NULL;

	for (elem = nacore_avl_tree_get_first(bridge->plugins);
	     index != 0;
	     elem = nacore_avl_tree_elem_get_next(bridge->plugins, elem),
	     index--) ;

	return &((nabrit_plugin)nacore_avl_tree_elem_get_value(
					bridge->plugins, elem))->descriptor;
}
