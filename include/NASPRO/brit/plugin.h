/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

/*
   Title: Plugins

   Bridged plugins.
 */

#ifndef _NASPRO_BRIDGE_IT_PLUGIN_H
#define _NASPRO_BRIDGE_IT_PLUGIN_H

#ifndef _NASPRO_BRIDGE_IT_LIB_H
# error Only <NASPRO/brit/lib.h> can be included directly.
#endif

NACORE_BEGIN_C_DECLS

/*
   Type: nabrit_plugin

   Plugin.
 */
typedef struct _nabrit_plugin * nabrit_plugin;

/*
   Function: nabrit_plugin_new()

   Creates a new plugin and adds it to the given plugin library of the given
   bridge.

   The function does only copy the LV2_Descriptor structure, but does not make
   an internal copy of the URI string, hence its existance until the destruction
   of the plugin must be ensured.

   Parameters:

     bridge	- Bridge.
     pluglib	- Plugin library.
     descriptor	- LV2 plugin descriptor for the bridged plugin.

   Returns:

     Plugin or NULL if some error occurred, in which case errno is set to ENOMEM
     if there was not enough memory or EEXIST if a plugin with the same URI was
     already added.
 */
_NABRIT_DEF nabrit_plugin
nabrit_plugin_new(nabrit_bridge bridge, nabrit_pluglib pluglib,
		  const LV2_Descriptor *descriptor);

/*
   Function: nabrit_plugin_free()

   Destroys a plugin.

   It does not destroy plugin ports and presets.

   Once this function is called, referring to plugin will cause undefined
   behavior.

   Parameters:

     bridge		- Bridge.
     pluglib		- Plugin library.
     plugin		- Plugin.
 */
_NABRIT_DEF void
nabrit_plugin_free(nabrit_bridge bridge, nabrit_pluglib pluglib,
		   nabrit_plugin plugin);

/*
   Function: nabrit_plugin_free_ports()

   Destroys the ports contained in a plugin.

   It does not destroy the content of ports, hence use free_cb for that.

   Once this function is called, referring to the ports contained in the plugin
   will cause undefined behavior.

   Parameters:

     plugin		- Plugin.
     free_cb		- Callback called for each port about to be destroied
			  (value is of type nabrit_port) or NULL.
     free_opaque	- Extra opaque data to be passed to free_cb or NULL.
 */
_NABRIT_DEF void
nabrit_plugin_free_ports(nabrit_plugin plugin, nacore_op_cb free_cb,
			 void *free_opaque);

/*
   Function: nabrit_plugin_free_presets()

   Destroys the presets associated to a plugin.

   Once this function is called, referring to the presets associated to a plugin
   will cause undefined behavior.

   Parameters:

     plugin		- Plugin.
     free_cb		- Callback called for each preset about to be destroied
			  (value is of type nabrit_preset) or NULL.
     free_opaque	- Extra opaque data to be passed to free_cb or NULL.
 */
_NABRIT_DEF void
nabrit_plugin_free_presets(nabrit_plugin plugin, nacore_op_cb free_cb,
			   void *free_opaque);

/*
   Function: nabrit_plugin_from_descriptor()

   Gets the plugin associated with the given descriptor.

   Parameters:

     descriptor	- LV2 plugin descriptor.

   Returns:

     Plugin associated with the given descriptor.
 */
_NABRIT_DEF nabrit_plugin
nabrit_plugin_from_descriptor(const LV2_Descriptor *descriptor);

/*
   Function: nabrit_plugin_get_descriptor()

   Gets a pointer to the LV2 plugin descriptor associated with the plugin.

   Parameters:

     plugin	- Plugin.

   Returns:

     Pointer to the LV2 plugin descriptor associated to the plugin.
 */
_NABRIT_DEF LV2_Descriptor *
nabrit_plugin_get_descriptor(nabrit_plugin plugin);

/*
   Function: nabrit_plugin_get_opaque()

   Gets opaque data associated with the plugin.

   Parameters:

     plugin	- Plugin.

   Returns:

     Opaque data associated to the plugin by the last call to
     <nabrit_plugin_set_opaque()> or NULL if such a call never happened before.
 */
_NABRIT_DEF void *
nabrit_plugin_get_opaque(nabrit_plugin plugin);

/*
   Function: nabrit_plugin_set_opaque()

   Associates arbitrary opaque data to a plugin library.

   Parameters:

     plugin	- Plugin.
     opaque	- Opaque data.
 */
_NABRIT_DEF void
nabrit_plugin_set_opaque(nabrit_plugin plugin, void *opaque);

/*
   Function: nabrit_plugin_set_name()

   Sets the plugin's name.

   Parameters:

     plugin	- Plugin.
     name	- Name.
 */
_NABRIT_DEF void
nabrit_plugin_set_name(nabrit_plugin plugin, const char *name);

/*
   Function: nabrit_plugin_set_creator()

   Sets the plugin creator's name.

   Parameters:

     plugin	- Plugin.
     creator	- Creator's name.
 */
_NABRIT_DEF void
nabrit_plugin_set_creator(nabrit_plugin plugin, const char *creator);

/*
   Function: nabrit_plugin_set_rights()

   Sets the plugin rights.

   Parameters:

     plugin	- Plugin.
     rights	- Rights.
 */
_NABRIT_DEF void
nabrit_plugin_set_rights(nabrit_plugin plugin, const char *rights);

/*
   Function: nabrit_plugin_set_is_live()

   Sets the plugin's "has live dependency" setting.

   Parameters:

     plugin	- Plugin.
     is_live	- 0 = no dependencies, non-0 = has dependency.
 */
_NABRIT_DEF void
nabrit_plugin_set_is_live(nabrit_plugin plugin, char is_live);

/*
   Function: nabrit_plugin_set_in_place_broken()

   Sets the plugin's "in place broken" setting.

   Parameters:

     plugin		- Plugin.
     in_place_broken	- 0 = no, non-0 = yes.
 */
_NABRIT_DEF void
nabrit_plugin_set_in_place_broken(nabrit_plugin plugin, char in_place_broken);

/*
   Function: nabrit_plugin_set_hard_rt_capable()

   Sets the plugin's "hard RT capable" setting.

   Parameters:

     plugin		- Plugin.
     hard_rt_capable	- 0 = no, non-0 = yes.
 */
_NABRIT_DEF void
nabrit_plugin_set_hard_rt_capable(nabrit_plugin plugin, char hard_rt_capable);

NACORE_END_C_DECLS

#endif
