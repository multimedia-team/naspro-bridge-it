/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

/*
   Title: Plugin libraries

   Bridged plugin libraries.
 */

#ifndef _NASPRO_BRIDGE_IT_PLUGLIB_H
#define _NASPRO_BRIDGE_IT_PLUGLIB_H

#ifndef _NASPRO_BRIDGE_IT_LIB_H
# error Only <NASPRO/brit/lib.h> can be included directly.
#endif

NACORE_BEGIN_C_DECLS

/*
   Type: nabrit_pluglib

   Plugin library.
 */
typedef struct _nabrit_pluglib * nabrit_pluglib;

/*
   Function: nabrit_pluglib_new()

   Creates a new plugin library and adds it to the given bridge.

   The function does not make an internal copy of the given filename, hence its
   existance until the destruction of the plugin library must be ensured.

   Parameters:

     bridge	- Bridge.
     filename	- Absolute filename of the bridged plugin library.

   Returns:

     Plugin library or NULL if there was not enough memory.
 */
_NABRIT_DEF nabrit_pluglib
nabrit_pluglib_new(nabrit_bridge bridge, const char *filename);

/*
   Function: nabrit_pluglib_free_plugins()

   Destroys the plugin references contained in a plugin library.

   It does not destroy the plugins and their contents, hence use free_cb for
   that.

   Once this function is called, referring to the plugins contained in the
   plugin library will cause undefined behavior.

   Parameters:

     bridge		- Bridge.
     pluglib		- Plugin library.
     free_cb		- Callback called for each plugin about to be destroied
			  (value is of type nabrit_plugin) or NULL.
     free_opaque	- Extra opaque data to be passed to free_cb or NULL.
 */
_NABRIT_DEF void
nabrit_pluglib_free_plugins(nabrit_bridge bridge, nabrit_pluglib pluglib,
			    nacore_op_cb free_cb, void *free_opaque);

/*
   Function: nabrit_pluglib_get_filename()

   Gets the filename associated to the given plugin library.

   The returned string must not be modified or free()d.

   Parameters:

     pluglib	- Plugin library.

   Returns:

     Filename.
 */
_NABRIT_DEF const char *
nabrit_pluglib_get_filename(nabrit_pluglib pluglib);

/*
   Function: nabrit_pluglib_get_opaque()

   Gets opaque data associated with the plugin library.

   Parameters:

     pluglib	- Plugin library.

   Returns:

     Opaque data associated to the plugin library by the last call to
     <nabrit_pluglib_set_opaque()> or NULL if such a call never happened before.
 */
_NABRIT_DEF void *
nabrit_pluglib_get_opaque(nabrit_pluglib pluglib);

/*
   Function: nabrit_pluglib_set_opaque()

   Associates arbitrary opaque data to a plugin library.

   Parameters:

     pluglib	- Plugin library.
     opaque	- Opaque data.
 */
_NABRIT_DEF void
nabrit_pluglib_set_opaque(nabrit_pluglib pluglib, void *opaque);

NACORE_END_C_DECLS

#endif
