/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

/*
   Title: Utilities

   For your convenience only(TM).
 */

#ifndef _NASPRO_BRIDGE_IT_UTIL_H
#define _NASPRO_BRIDGE_IT_UTIL_H

#ifndef _NASPRO_BRIDGE_IT_LIB_H
# error Only <NASPRO/brit/lib.h> can be included directly.
#endif

NACORE_BEGIN_C_DECLS

/*
   Function: nabrit_util_filter_by_suffix()

   A filter function that filters away strings not ending with a given suffix.

   Parameters:

     value	- Input string.
     opaque	- Suffix string.

   Returns:

     non-0 if strlen(value) >= strlen(opaque) and the tail of value matches with
     opaque, 0 otherwise.
 */
_NABRIT_DEF char
nabrit_util_filter_by_suffix(const void *value, void *opaque);

/*
   Function: nabrit_util_load_all_in_env_path()

   Loads all plugin libraries using the specified environment variable as a list
   of directories to be scanned.

   Each directory entry that is not filtered out by dir_entry_filter_cb, if one
   is given, is passed to the load_cb callback.

   dir_entry_filter_cb is passed a directory entry whose type is
   nacore_fs_dir_entry, that must not be destroied.

   load_cb is passed a malloc()-allocated string containing the absoulte
   filename of the found entry. This string is not free()d, hence you will be
   responsible of its lifecycle.

   The function is designed to be quite indulgent w.r.t. errors that don't stop
   it from keep on running, hence some errors won't result in the whole
   operation failing.

   Parameters:

     bridge			- Bridge.
     name			- Environment variable name.
     dir_entry_filter_cb	- Directory entry filter callback or NULL.
     dir_entry_filter_opaque	- Extra opaque data to be passed to
				  dir_entry_filter_cb or NULL.
     load_cb			- Plugin library loader callback or NULL.
     load_opaque		- Extra opaque data to be passed to load_cb or
				  NULL.

   Returns:

     0 on success, ENOMEM if there was not enough memory, ENOENT if the
     specified environment variable was not set or NACORE_EUNKNOWN if another
     kind of error happened.
 */
_NABRIT_DEF int
nabrit_util_load_all_in_env_path(nabrit_bridge bridge, const char *name,
				 nacore_filter_cb dir_entry_filter_cb,
				 void *dir_entry_filter_opaque,
				 nacore_op_cb load_cb, void *load_opaque);

/*
   Function: nabrit_util_load_all_in_dir()

   Loads all plugin libraries in a directory.

   Each directory entry that is not filtered out by dir_entry_filter_cb, if one
   is given, is passed to the load_cb callback.

   dir_entry_filter_cb is passed a directory entry whose type is
   nacore_fs_dir_entry, that must not be destroied.

   load_cb is passed a malloc()-allocated string containing the absoulte
   filename of the found entry. This string is not free()d, hence you will be
   responsible of its lifecycle.

   The function is designed to be quite indulgent w.r.t. errors that don't stop
   it from keep on running, hence some errors won't result in the whole
   operation failing.

   Parameters:

     bridge			- Bridge.
     dirname			- Absolute path name (without trailing
				  separator).
     dir_entry_filter_cb	- Directory entry filter callback or NULL.
     dir_entry_filter_opaque	- Extra opaque data to be passed to
				  dir_entry_filter_cb or NULL.
     load_cb			- Plugin library loader callback or NULL.
     load_opaque		- Extra opaque data to be passed to load_cb or
				  NULL.

   Returns:

     0 on success, EACCESS if permission denied, NACORE_ELOOP if there is a loop
     in symbolic links or there were too many links to be solved, ENAMETOOLONG
     if the length of a component of the path name is too long or the
     intermediate result of a symbolic link was too long, ENOENT if the
     specified directory does not exist, ENOTDIR if the specified file is not a
     directory, EMFILE if there are too many open files for the process, ENFILE
     if there are too many open files in the system or NACORE_EUNKNOWN if
     another kind of error happened.
 */
_NABRIT_DEF int
nabrit_util_load_all_in_dir(nabrit_bridge bridge, const char *dirname,
			    nacore_filter_cb dir_entry_filter_cb,
			    void *dir_entry_filter_opaque, nacore_op_cb load_cb,
			    void *load_opaque);

NACORE_END_C_DECLS

#endif
