/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

/*
   Title: NASPRO Bridge it

   *Version*: 0.5.1, *API*: 3.0.0.

   About:

     <NASPRO Bridge it at
     http://naspro.sourceforge.net/libraries.html#naspro-bridge-it> is a little
     helper library to develop <insert-your-API-here> to LV2 bridges.

     As of now, it basically offers a few utility functions and Turtle/RDF
     serialization for LV2 dynamic manifest generation, supporting the following
     LV2 extensions.

       * <LV2 at http://lv2plug.in/ns/lv2core/> >= 4.0;
       * <LV2 Dynamic Manifest at http://lv2plug.in/ns/ext/dynmanifest/> >= 1.0;
       * <LV2 Port Properties at http://lv2plug.in/ns/ext/port-props/> >= 1.0
         (only for the logarithmic port property);
       * <LV2 Atom at http://lv2plug.in/ns/ext/atom/> >= 1.0;
       * <LV2 MIDI at http://lv2plug.in/ns/ext/midi/> >= 1.0;
       * <LV2 URID at http://lv2plug.in/ns/ext/urid/> >= 1.2;
       * <LV2 Presets at http://lv2plug.in/ns/ext/presets/> >= 2.6.

   Supported platforms/compilers:

     It is written in C99 and there is no platform- or compiler-specific code,
     hence wherever <NASPRO core at
     http://naspro.sourceforge.net/libraries.html#naspro-core> runs, this
     library is able to run.

   API conventions:

     * No function should be assumed to be thread-safe, unless otherwise
       specified;
     * All strings are null-terminated and UTF-8 encoded according to the
       Unicode 6.0 standard and without BOM (byte-order-mark) characters, unless
       otherwise specified;
     * No function does input validation, hence, in case the API is misused in
       this sense, the results are undefined.

   Runtime dependencies:

     * NASPRO core >= 0.4.0.

   Build-time dependencies:

     * <LV2 at http://lv2plug.in> >= 1.0.0;
     * <pkg-config at http://pkg-config.freedesktop.org/>;
     * An environment capable of running Autotools-based build systems;
     * (optional) <GNU Autoconf at http://www.gnu.org/software/autoconf/> >=
       2.69, <GNU Automake at http://www.gnu.org/software/automake/> and
       <GNU Libtool at http://www.gnu.org/software/libtool/> to regenerate the
       build system;
     * (optional) <Natural Docs at http://www.naturaldocs.org/> >= 1.5 to
       regenerate the build system and/or the documentation.

   Usage:

      Just include <NASPRO/brit/lib.h> in your source files and use the
      pkg-config tool to retrieve compiler and linker flags (package name:
      nabrit-3).
 */

#ifndef _NASPRO_BRIDGE_IT_LIB_H
#define _NASPRO_BRIDGE_IT_LIB_H

#include <stdio.h>

#include "lv2/lv2plug.in/ns/lv2core/lv2.h"

#include <NASPRO/core/lib.h>

#ifdef _NABRIT_INTERNAL_H
# define _NABRIT_DEF		NACORE_PUBLIC NACORE_EXPORT
#else
# define _NABRIT_DEF		NACORE_PUBLIC NACORE_IMPORT
#endif

#include <NASPRO/brit/bridge.h>
#include <NASPRO/brit/pluglib.h>
#include <NASPRO/brit/plugin.h>
#include <NASPRO/brit/port.h>
#include <NASPRO/brit/preset.h>
#include <NASPRO/brit/manifest.h>
#include <NASPRO/brit/util.h>

#endif
