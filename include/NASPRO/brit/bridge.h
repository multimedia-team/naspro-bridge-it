/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

/*
   Title: Bridges

   Bridging LV2 plugin libraries.
 */

#ifndef _NASPRO_BRIDGE_IT_BRIDGE_H
#define _NASPRO_BRIDGE_IT_BRIDGE_H

#ifndef _NASPRO_BRIDGE_IT_LIB_H
# error Only <NASPRO/brit/lib.h> can be included directly.
#endif

NACORE_BEGIN_C_DECLS

/*
   Type: nabrit_bridge

   Bridge.
 */
typedef struct _nabrit_bridge	* nabrit_bridge;

/*
   Function: nabrit_bridge_new()

   Creates a new bridge.

   Parameters:

     binary	- Filename relative to the LV2 bundle (i.e., object to
		  lv2:binary predicate in the dynamic manifest).

   Returns:

     Bridge or NULL if there was not enough memory.
 */
_NABRIT_DEF nabrit_bridge
nabrit_bridge_new(const char *binary);

/*
   Function: nabrit_bridge_free()

   Destroys a bridge and the plugin libraries it contains.

   It does not destroy the content of plugin libraries, hence use free_cb for
   that.

   Once this function is called, referring to bridge will cause undefined
   behavior.

   Parameters:

     bridge		- Bridge.
     free_cb		- Callback called for each plugin library about to be
			  destroied (value is of type nabrit_pluglib) or NULL.
     free_opaque	- Extra opaque data to be passed to free_cb or NULL.
 */
_NABRIT_DEF void
nabrit_bridge_free(nabrit_bridge bridge, nacore_op_cb free_cb,
		   void *free_opaque);

/*
   Function: nabrit_bridge_get_descriptor()

   Gets an LV2 plugin descriptor by index.

   The association of index (0 to number of plugins-1) to plugins is
   determined by an internal ordering. As long as no plugins are added or
   removed between two calls, the same index will correspond to the same plugin.

   Parameters:

     bridge	- Bridge.
     index	- Index.

   Returns:

     LV2 plugin descriptor or NULL if index >= number of plugins.
 */
_NABRIT_DEF const LV2_Descriptor *
nabrit_bridge_get_descriptor(nabrit_bridge bridge, uint32_t index);

NACORE_END_C_DECLS

#endif
