/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

/*
   Title: Presets

   Presets.
 */

#ifndef _NASPRO_BRIDGE_IT_PRESET_H
#define _NASPRO_BRIDGE_IT_PRESET_H

#ifndef _NASPRO_BRIDGE_IT_LIB_H
# error Only <NASPRO/brit/lib.h> can be included directly.
#endif

NACORE_BEGIN_C_DECLS

/*
   Type: nabrit_preset

   Preset.
 */
typedef struct _nabrit_preset * nabrit_preset;

/*
   Function: nabrit_preset_new()

   Creates a new preset associated to the given plugin.

   The function does not make an internal copy of the given preset name, hence
   its existance until the destruction of the plugin must be ensured.

   Parameters:

     plugin	- Plugin.
     name	- Preset name.

   Returns:

     New preset or NULL if there was not enough memory.
 */
_NABRIT_DEF nabrit_preset
nabrit_preset_new(nabrit_plugin plugin, const char *name);

/*
   Function: nabrit_preset_add_value()

   Adds a port value to the preset.

   Parameters:

     preset	- Preset.
     port	- Port.
     value	- Value.

   Returns:

     0 on success or ENOMEM if there was not enough memory.
 */
_NABRIT_DEF int
nabrit_preset_add_value(nabrit_preset preset, nabrit_port port, float value);

/*
   Function: nabrit_preset_get_name()

   Gets the preset name.

   Parameters:

     preset	- Preset.

   Returns:

     Preset name.
 */
_NABRIT_DEF const char *
nabrit_preset_get_name(nabrit_preset preset);

NACORE_END_C_DECLS

#endif
