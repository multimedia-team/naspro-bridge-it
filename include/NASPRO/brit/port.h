/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

/*
   Title: Plugin ports

   Bridged plugin ports.
 */

#ifndef _NASPRO_BRIDGE_IT_PORT_H
#define _NASPRO_BRIDGE_IT_PORT_H

#ifndef _NASPRO_BRIDGE_IT_LIB_H
# error Only <NASPRO/brit/lib.h> can be included directly.
#endif

NACORE_BEGIN_C_DECLS

/*
   Enum: nabrit_port_type

   Port type.

     nabrit_port_type_control	- Control port.
     nabrit_port_type_audio	- Audio port.
     nabrit_port_type_midi	- MIDI port.
 */
typedef enum
  {
	nabrit_port_type_control,
	nabrit_port_type_audio,
	nabrit_port_type_midi
  } nabrit_port_type;

/*
   Enum: nabrit_port_direction

   Port direction.

     nabrit_port_direction_in	- Input port.
     nabrit_port_direction_out	- Output port.
 */
typedef enum
  {
	nabrit_port_direction_in,
	nabrit_port_direction_out
  } nabrit_port_direction;

/*
   Type: nabrit_port

   Plugin port.
 */
typedef struct _nabrit_port * nabrit_port;

/*
   Function: nabrit_port_new()

   Creates a new plugin port and adds it to the given plugin.

   The function does not make an internal copy of the given port symbol, hence
   its existance until the destruction of the plugin must be ensured.

   Parameters:

     plugin	- Plugin.
     symbol	- Port symbol.
     type	- Port type.
     direction	- Port direction.

   Returns:

     New plugin port or NULL if there was not enough memory.
 */
_NABRIT_DEF nabrit_port
nabrit_port_new(nabrit_plugin plugin, const char *symbol, nabrit_port_type type,
		nabrit_port_direction direction);

/*
   Function: nabrit_port_get_symbol()

   Gets the port's symbol.

   Parameters:

     port	- Plugin port.

   Returns:

     The port's symbol.
 */
_NABRIT_DEF const char *
nabrit_port_get_symbol(nabrit_port port);

/*
   Function: nabrit_port_get_type()

   Gets the port's type.

   Parameters:

     port	- Plugin port.

   Returns:

     The port's type.
 */
_NABRIT_DEF nabrit_port_type
nabrit_port_get_type(nabrit_port port);

/*
   Function: nabrit_port_set_name()

   Sets the port's name.

   Parameters:

     port	- Plugin port.
     name	- Name.
 */
_NABRIT_DEF void
nabrit_port_set_name(nabrit_port port, const char *name);

/*
   Function: nabrit_port_set_reports_latency()

   Sets the port's "reports latency" setting.

   Parameters:

     port		- Plugin port.
     reports_latency	- 0 = no, non-0 = yes.
 */
_NABRIT_DEF void
nabrit_port_set_reports_latency(nabrit_port port, char reports_latency);

/*
   Function: nabrit_port_set_toggled()

   Sets the port's "toggled" setting.

   Parameters:

     port	- Plugin port.
     toggled	- 0 = no, non-0 = yes.
 */
_NABRIT_DEF void
nabrit_port_set_toggled(nabrit_port port, char toggled);

/*
   Function: nabrit_port_set_sample_rate()

   Sets the port's "per sample rate" setting.

   Parameters:

     port		- Plugin port.
     sample_rate	- 0 = no, non-0 = yes.
 */
_NABRIT_DEF void
nabrit_port_set_sample_rate(nabrit_port port, char sample_rate);

/*
   Function: nabrit_port_set_integer()

   Sets the port's "integer values" setting.

   Parameters:

     port	- Plugin port.
     integer	- 0 = no, non-0 = yes.
 */
_NABRIT_DEF void
nabrit_port_set_integer(nabrit_port port, char integer);

/*
   Function: nabrit_port_set_logarithmic()

   Sets the port's "logarithmic scale" setting.

   Parameters:

     port		- Plugin port.
     logarithmic	- 0 = no, non-0 = yes.
 */
_NABRIT_DEF void
nabrit_port_set_logarithmic(nabrit_port port, char logarithmic);

/*
   Function: nabrit_port_set_min()

   Sets the port's minimum value.

   Parameters:

     port	- Plugin port.
     min	- Minimum value.
 */
_NABRIT_DEF void
nabrit_port_set_min(nabrit_port port, float min);

/*
   Function: nabrit_port_set_max()

   Sets the port's maximum value.

   Parameters:

     port	- Plugin port.
     max	- Maximum value.
 */
_NABRIT_DEF void
nabrit_port_set_max(nabrit_port port, float max);

/*
   Function: nabrit_port_set_deflt()

   Sets the port's default value.

   Parameters:

     port	- Plugin port.
     def	- Default value.
 */
_NABRIT_DEF void
nabrit_port_set_deflt(nabrit_port port, float deflt);

NACORE_END_C_DECLS

#endif
