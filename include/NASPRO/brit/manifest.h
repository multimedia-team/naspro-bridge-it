/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * LV2 bridging helper library
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

/*
   Title: Manifest

   Dynamic manifest generator helpers.
 */

#ifndef _NASPRO_BRIDGE_IT_MANIFEST_H
#define _NASPRO_BRIDGE_IT_MANIFEST_H

#ifndef _NASPRO_BRIDGE_IT_LIB_H
# error Only <NASPRO/brit/lib.h> can be included directly.
#endif

NACORE_BEGIN_C_DECLS

/*
   Function: nabrit_manifest_print_subjects()

   Prints URI "exposition" Turtle/RDF triples on fp.

   Parameters:

     bridge	- Bridge.
     fp		- File pointer.
 */
_NABRIT_DEF void
nabrit_manifest_print_subjects(nabrit_bridge bridge, FILE *fp);

/*
   Function: nabrit_manifest_print_data()

   Prints Turtle/RDF triples regarding the subject specified by URI on fp.

   Parameters:

     bridge	- Bridge.
     fp		- File pointer.
     URI	- Subject URI.

   Returns:

     0 on success, ENOMEM if there was not enough memory or ENOENT if the
     specified subject URI is unknown.
 */
_NABRIT_DEF int
nabrit_manifest_print_data(nabrit_bridge bridge, FILE *fp, const char *URI);

NACORE_END_C_DECLS

#endif
